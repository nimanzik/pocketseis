# PocketSeis - a pocket tool for seismology

![](https://img.shields.io/badge/licence-GPL--3.0-orange)

This repository contains Python modules and scripts to facilitate repetitive
tasks in seismological projects.

## Dependencies

- Python (>= 3.6, with development headers)
- Matplotlib (>= 3.4.3)
- NumPy (>= 1.21.2)
- SciPy (>= 1.7.1)
- [PyTorch](https://pytorch.org/) (>= 1.9.1)
- xarray (>= 0.19.0)
- [Pyrocko](https://pyrocko.org/) (>= 2021.09.14)

## Download and Installation

```shell
git clone https://github.com/nimanzik/pocketseis
cd pocketseis
pip install .
```

or use `pip install -e .` to install in editable (developer) mode.
